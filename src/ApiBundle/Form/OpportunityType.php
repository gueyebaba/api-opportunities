<?php

namespace ApiBundle\Form;

use BusinessBundle\Entity\ValueList;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OpportunityType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('fullDescription', TextType::class)
            ->add('tags', TextType::class)
            ->add('industries',  EntityType::class, array(
                'class' => ValueList::class,
                'multiple' => true
            ))

            ->add('counterparts',  EntityType::class, array(
                'class' => ValueList::class,
                'multiple' => true
            ))
            ->add('typeOfDesired', TextType::class)
            ->add('media', TextType::class)
            ->add('website',TextType::class)
            ->add('linkToSocialMedia', TextType::class)
            ->add('location', TextType::class)
            ->add('country', TextType::class)
            ->add('status', TextType::class)
            ->add('companyLogo', TextType::class)
            ->add('companyName', TextType::class)
            ->add('companyWebsite', TextType::class)
            ->add('companyCity', TextType::class)
            ->add('companyCountry', TextType::class)
            ->add('companyDescription', TextType::class)
            ->add('userFirstName', TextType::class)
            ->add('userLastName', TextType::class)
            ->add('userEmail', TextType::class)
            ->add('userId', TextType::class)
            ->add('closedAt',DateTimeType::class, array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'required' => 'false',
                'format' => 'YYYY-MM-dd HH:mm',
                'attr' => array('data-date-format' => 'YYYY-MM-DD HH:mm')
            ))
            ->add('publishRequestAt', DateTimeType::class, array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'required' => 'false',
                'format' => 'YYYY-MM-dd HH:mm',
                'attr' => array('data-date-format' => 'YYYY-MM-DD HH:mm')
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BusinessBundle\Entity\Opportunity',
            'csrf_protection' => false,
            'method' => 'PATCH'
        ));
    }
}
