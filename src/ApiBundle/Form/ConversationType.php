<?php

namespace ApiBundle\Form;

use BusinessBundle\Entity\Opportunity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConversationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('messageRoot', TextType::class)
            ->add('content', TextType::class)
            ->add('company', TextType::class)
            ->add('companyLogo', TextType::class)
            ->add('companyName', TextType::class)
            ->add('companyCity', TextType::class)
            ->add('companyCountry', TextType::class)
            ->add('companyWebsite', TextType::class)
            ->add('description', TextType::class)
            ->add('userFirstName', TextType::class)
            ->add('userLastName', TextType::class)
            ->add('userEmail', TextType::class)
            ->add('status', TextType::class)
            ->add('opportunity', EntityType::class, array(
                'class' => Opportunity::class
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BusinessBundle\Entity\Conversation',
            'csrf_protection' => false,
            'method' => 'PATCH'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
}
