<?php

namespace ApiBundle\Form;

use ApiBundle\Form\DataTransformer\TextToDateTimeDataTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OpportunityParametersType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('industries', TextType::class)
            ->add('counterparts', TextType::class)
            ->add('userEmail', TextType::class)
            ->add('companyName', TextType::class)
            ->add('creationDate', TextType::class)
            ->add('location', TextType::class)
            ->add('limit', TextType::class)
            ->add('offset', TextType::class)
            ->add('sortBy', TextType::class)
            ->add('sortDir', TextType::class)
            ->add('closingDateFrom', DateTimeType::class, array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'required' => 'false',
                'format' => 'YYYY-MM-dd HH:mm',
                'attr' => array('data-date-format' => 'YYYY-MM-DD HH:mm')
            ))
            ->add('closingDateTo', DateTimeType::class, array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'required' => 'false',
                'format' => 'YYYY-MM-dd HH:mm',
                'attr' => array('data-date-format' => 'YYYY-MM-DD HH:mm')
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BusinessBundle\ValueObject\OpportunityParameters',
            'csrf_protection' => false
        ));
    }
}
