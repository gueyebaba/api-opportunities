<?php

namespace ApiBundle\Controller\Pub;

use ApiBundle\Form\OpportunityParametersType;
use ApiBundle\Form\OpportunityType;
use BusinessBundle\Entity\Conversation;
use BusinessBundle\Entity\Opportunity;
use BusinessBundle\ValueObject\OpportunityParameters;
use Ee\EeCommonBundle\Exception\BusinessException;
use Ee\EeCommonBundle\Service\Validation\Form\FormBusinessException;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpKernel\Exception\HttpException;


class OpportunitiesController extends FOSRestController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="return list of paginate opportunities",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Opportunity::class, groups={"opportunity"}))
     *     ),
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     * ),
     * @SWG\Parameter(
     *      name="X-CUSTOMER-REF",
     *      in="header",
     *      type="string",
     *      required=true,
     * ),
     * @SWG\Parameter(
     *      name="X-SCOPE",
     *      in="header",
     *      type="string",
     *      required=true,
     * ),
     * @SWG\Parameter(
     *      name="login",
     *      in="header",
     *      type="string",
     *      required=true,
     * ),
     * @SWG\Parameter(
     *      name="password",
     *      in="header",
     *      type="string",
     *      required=true,
     * ),
     * @SWG\Parameter(
     *      name="industries",
     *      in="query",
     *      type="array",
     *  @SWG\Items(
     *      type="string"
     *  )
     * ),
     * @SWG\Parameter(
     *      name="counterparts",
     *      in="query",
     *      type="array",
     *  @SWG\Items(
     *      type="string"
     *  )
     * )
     * @Rest\QueryParam(name="userEmail", strict=false,  nullable=true)
     * @Rest\QueryParam(name="companyName", strict=false,  nullable=true)
     * @Rest\QueryParam(name="creationDate", strict=false,  nullable=true)
     * @Rest\QueryParam(name="location", strict=false,  nullable=true)
     * @Rest\QueryParam(name="closingDateFrom", strict=false, nullable=true)
     * @Rest\QueryParam(name="closingDateTo", strict=false, nullable=true)
     * @Rest\QueryParam(name="limit", strict=false,  nullable=true)
     * @Rest\QueryParam(name="offset", strict=false,  nullable=true)
     * @Rest\QueryParam(name="sortBy", allowBlank=false, default="creationDate", description="Sort field")
     * @Rest\QueryParam(name="sortDir", requirements="(asc|desc)+", allowBlank=false, default="desc", description="Sort direction")
     *
     * @SWG\Tag(name="Public")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function listAction(Request $request)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        try {
            $opportunityParameters = $this->get('api.opportunity_value_object');
            $defaultOffset = $opportunityParameters->getOffset();
            $defaultLimit = $opportunityParameters->getLimit();
            $form = $this->createForm(OpportunityParametersType::class, $opportunityParameters, ['method' => $request->getMethod()]);
            $form->handleRequest($request);
            $this->get('ee.form.validator')->validate($form);
            $filterParams = $opportunityParameters->toArray();
            $query = $this->get('api.opportunity_manager')->getOpportunities($filterParams);
            $limit = (empty($opportunityParameters->getLimit())) ? $defaultLimit : $opportunityParameters->getLimit();
            $offset = (empty($opportunityParameters->getOffset())) ? $defaultOffset : $opportunityParameters->getOffset();

            $context = new Context();
            $groups = ['opportunity', 'message'];
            $context->setGroups($groups);

            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query,
                (int)($offset / $limit) + 1,
                $limit
            );

            $opportunities = $pagination->getItems();

            $response =[
                "total" => $pagination->getTotalItemCount(),
                "opportunities" => $opportunities
            ];

            $view = $this->view($response, $responseCode);
            $view->setContext($context);

            return $this->handleView($view);

        } catch(FormBusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $opportunities = $ex->getPayload();
            $responseCode = Response::HTTP_NOT_ACCEPTABLE;
        }

        return $this->view($opportunities, $responseCode);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="return one opportunity",
     *     @SWG\Items(ref=@Model(type=Opportunity::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Public")
     * @ParamConverter("opportunity", converter="doctrine.orm")
     * @throws \Exception
     */
    public function getOneAction(Opportunity $opportunity)
    {
        $responseCode = Response::HTTP_OK;
        $context = new Context();
        $groups = ['opportunity', 'message'];
        $context->setGroups($groups);
        $view = $this->view($opportunity, $responseCode);
        $view->setContext($context);

        return $this->handleView($view);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Create an opportunity",
     *     @SWG\Items(ref=@Model(type=Opportunity::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=406,
     *     description="Error in form validation",
     *     examples={
     *          "Mandatory field":{
     *               "title":
     *                   {{
     *                       "required_options": {
     *                           "name",
     *                           "code",
     *                           "message"
     *                       },
     *                       "name": "title",
     *                       "rule": "NotBlankValidator",
     *                       "code": "IS_BLANK_ERROR",
     *                       "message": "title value is required"
     *                   }
     *          }}
     *     }
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *     name="body",
     *     description="....",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="title",
     *             type="string",
     *             example="Get up to 2% off the cost of transporting your goods"
     *         ),
     *        @SWG\Property(
     *             property="fullDescription",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="industries",
     *             type="array",
     *             collectionFormat="multi",
     *             @SWG\Items(
     *                 type="integer",
     *            )
     *         ),
     *         @SWG\Property(
     *             property="counterparts",
     *             type="array",
     *             collectionFormat="multi",
     *             @SWG\Items(
     *                 type="integer",
     *            )
     *         ),
     *         @SWG\Property(
     *             property="tags",
     *             type="array",
     *             collectionFormat="multi",
     *             @SWG\Items(
     *                 type="string",
     *            )
     *         ),
     *         @SWG\Property(
     *             property="typeOfDesired",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="media",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="website",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="linkToSocialMedia",
     *             type="string",
     *         ),
     *        @SWG\Property(
     *             property="country",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="location",
     *             type="string"
     *         ),
     *         @SWG\Property(
     *             property="companyDescription",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyLogo",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyWebsite",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyCity",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyCountry",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userFirstName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userLastName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userEmail",
     *             type="string",
     *         )
     *     )
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Public")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function createAction(Request $request, $user_id)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');

        $opportunity = new Opportunity();
        try {
            $form = $this->createForm(OpportunityType::class, $opportunity, ['method' => $request->getMethod()]);
            $form->handleRequest($request);
            $this->get('ee.form.validator')->validate($form);
            $this->get('app_logger')->logInfo('Opportunity Creation', ['Title' => $opportunity->getTitle()]);

            $opportunity->setUserId($user_id);
            $opportunity->setStatus("draft");

            $this->get('api.opportunity_manager')->save($opportunity);

        } catch(FormBusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $opportunity = $ex->getPayload();
            $responseCode = Response::HTTP_NOT_ACCEPTABLE;
        }

        return $this->view($opportunity, $responseCode);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Update an opportunity before publication",
     *     @SWG\Items(ref=@Model(type=Opportunity::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     *
     * ),
     * @SWG\Response(
     *     response=406,
     *     description="Error in form validation",
     *
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *     name="body",
     *     description="....",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="title",
     *             type="string",
     *         ),
     *        @SWG\Property(
     *             property="fullDescription",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="tags",
     *             type="array",
     *             collectionFormat="multi",
     *             @SWG\Items(
     *                 type="string",
     *            )
     *         ),
     *        @SWG\Property(
     *             property="industries",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="typeOfDesired",
     *             type="string",
     *         ),
     *        @SWG\Property(
     *             property="counterparts",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="media",
     *             type="string",
     *         ),
     *        @SWG\Property(
     *             property="website",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="linkToSocialMedia",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *         ),
     *        @SWG\Property(
     *             property="country",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="location",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyDescription",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyLogo",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyWebsite",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyCity",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyCountry",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userFirstName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userLastName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userEmail",
     *             type="string",
     *         )
     *     )
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Public")
     * @ParamConverter("opportunity", converter="doctrine.orm")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function updateAction(Request $request, Opportunity $opportunity, $userId)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');

        if ($userId !== $opportunity->getUserId()) {
            throw new HttpException(Response::HTTP_NOT_FOUND,'User not found');
        }
        try {
            $form = $this->createForm(OpportunityType::class, $opportunity, ['method' => $request->getMethod()]);
            $form->handleRequest($request);
            $this->get('ee.form.validator')->validate($form);
            $opportunity->setUserId($userId);
            $this->get('app_logger')->logInfo('Opportunity update', ['Title' => $opportunity->getTitle(), 'Total message' => count($opportunity->getThreads())]);
            $this->get('api.opportunity_manager')->save($opportunity, true);
        } catch(FormBusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $opportunity = $ex->getPayload();
            $responseCode = Response::HTTP_NOT_ACCEPTABLE;
        }

        return $this->view($opportunity, $responseCode);
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Update an opportunity before publication",
     *     @SWG\Items(ref=@Model(type=Opportunity::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=406,
     *     description="Error in form validation",
     *
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *     name="body",
     *     description="....",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *         )
     *     )
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Public")
     * @ParamConverter("opportunity", converter="doctrine.orm")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function updateStatusAction(Request $request, Opportunity $opportunity, $user_id)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');

        if($opportunity->getStatus() !== Opportunity::UPDATE_STATUS || $user_id !== $opportunity->getUserId()) {
            throw new HttpException(Response::HTTP_NOT_FOUND,'User not found or opportunity\'s different to draft');
        }
        try {
            $form = $this->createForm(OpportunityType::class, $opportunity, ['method' => $request->getMethod()]);
            $form->handleRequest($request);
            $this->get('ee.form.validator')->validate($form);
            $opportunity->setUserId($user_id);
            $this->get('app_logger')->logInfo('Update Status', ['Title' => $opportunity->getTitle(), 'Status' => $opportunity->getStatus()]);
            $this->get('api.opportunity_manager')->save($opportunity, true);

        } catch(FormBusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $opportunity = $ex->getPayload();
            $responseCode = Response::HTTP_NOT_ACCEPTABLE;
        }

        return $this->view($opportunity, $responseCode);
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Update an opportunity before publication",
     *     @SWG\Items(ref=@Model(type=Opportunity::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     *
     * ),
     * @SWG\Response(
     *     response=406,
     *     description="Error in form validation",
     *
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *     name="body",
     *     description="....",
     *     in="body",
     *     @SWG\Schema(
     *        @SWG\Property(
     *             property="closedAt",
     *             type="string",
     *             example="2018-10-11 10:11:00"
     *         )
     *     )
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Public")
     * @ParamConverter("opportunity", converter="doctrine.orm")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function updateClosedAtAction(Request $request, Opportunity $opportunity, $user_id)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');

        if($opportunity->getStatus() === Opportunity::EXPIRATION_DATE_STATUS_PUBLISH || $opportunity->getStatus() === Opportunity::EXPIRATION_DATE_STATUS_EXPIRED && $user_id === $opportunity->getUserId()) {
            try {
                $form = $this->createForm(OpportunityType::class, $opportunity, ['method' => $request->getMethod()]);
                $form->handleRequest($request);
                $this->get('ee.form.validator')->validate($form);
                $opportunity->setUserId($user_id);
                $this->get('app_logger')->logInfo('Update Status', ['Title' => $opportunity->getTitle(), 'Status' => $opportunity->getStatus()]);
                $this->get('api.opportunity_manager')->save($opportunity, true);

            } catch(FormBusinessException $ex) {
                $logger->logError($ex->getMessage(), $ex);
                $opportunity = $ex->getPayload();
                $responseCode = Response::HTTP_NOT_ACCEPTABLE;
            }
        } else{
            throw new HttpException(Response::HTTP_NOT_FOUND,'User not found or opportunity\'s status different to published ou expired');
        }

        return $this->view($opportunity, $responseCode);
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Update an opportunity before publication",
     *     @SWG\Items(ref=@Model(type=Opportunity::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     *
     * ),
     * @SWG\Response(
     *     response=406,
     *     description="Error in form validation",
     *
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *     name="body",
     *     description="....",
     *     in="body",
     *     @SWG\Schema(
     *        @SWG\Property(
     *             property="publishRequestAt",
     *             type="string",
     *             example="2018-10-11 10:11:00",
     *             format="date-time"
     *         )
     *     )
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Public")
     * @ParamConverter("opportunity", converter="doctrine.orm")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function updatePublishRequestAtAction(Request $request, Opportunity $opportunity, $user_id)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');

        if($user_id === $opportunity->getUserId()) {
            try {
                $form = $this->createForm(OpportunityType::class, $opportunity, ['method' => $request->getMethod()]);
                $form->handleRequest($request);
                $this->get('ee.form.validator')->validate($form);
                $opportunity->setUserId($user_id);
                $this->get('app_logger')->logInfo('Update publish request date', ['Title' => $opportunity->getTitle(), 'publish request date' => $opportunity->getPublishRequestAt()]);
                $this->get('api.opportunity_manager')->save($opportunity, true);
            }catch(FormBusinessException $ex) {
                $logger->logError($ex->getMessage(), $ex);
                $opportunity = $ex->getPayload();
                $responseCode = Response::HTTP_NOT_ACCEPTABLE;
            }
        }else{
            throw new HttpException(Response::HTTP_NOT_FOUND,'User not found');
        }

        return $this->view($opportunity, $responseCode);
    }

    /**
     * @SWG\Response(
     *     response=204,
     *     description="Request success empty content",
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     *
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Public")
     * @ParamConverter("opportunity", converter="doctrine.orm")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function deleteAction(Opportunity $opportunity, $user_id)
    {
        if($opportunity->getStatus() !== Opportunity::DELETE_STATUS || $user_id !== $opportunity->getUserId()) {
            throw new HttpException(Response::HTTP_NOT_FOUND,'User not found or opportunity\'s different to draft');
        }

        $this->get('app_logger')->logInfo('Deleted Opportunity', ['Title' => $opportunity->getTitle()]);
        $this->get('api.opportunity_manager')->remove($opportunity);

        return $this->view($opportunity, Response::HTTP_NO_CONTENT);
    }
}