<?php

namespace ApiBundle\Controller\Pub;

use ApiBundle\Form\ConversationType;
use BusinessBundle\Entity\Opportunity;
use BusinessBundle\Entity\Conversation;
use Ee\EeCommonBundle\Exception\BusinessException;
use Ee\EeCommonBundle\Service\Validation\Form\FormBusinessException;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Request\ParamFetcher;


class ConversationController extends FOSRestController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Reply an opportunity",
     *     @SWG\Items(ref=@Model(type=Conversation::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=204,
     *     description="Request success empty content",
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=406,
     *     description="Error in form validation",
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     * ),
     * @SWG\Parameter(
     *     name="body",
     *     description="....",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="messageRoot",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="content",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="company",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyLogo",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyCity",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyCountry",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyWebsite",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="description",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userFirstName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userLastName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userEmail",
     *             type="string",
     *         )
     *     )
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Public")
     * @ParamConverter("opportunity", converter="doctrine.orm")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function replyOpportunityAction(Request $request, Opportunity $opportunity, $user_id)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        $conversation = new Conversation();
        try {
            $form = $this->createForm(ConversationType::class, $conversation, ['method' => $request->getMethod()]);
            $form->handleRequest($request);
            $this->get('ee.form.validator')->validate($form);
            $conversation->setStatus("publishRequest");
            $conversation->setUserId($user_id);
            $conversation->setOpportunity($opportunity);
            $this->get('api.conversation_manager')->save($conversation);

        }catch(FormBusinessException $ex) {
            foreach ($ex->getPayload() as $value){
                $logger->logInfo($value[0]->getMessage());
            }
            $logger->logError($ex->getMessage(), $ex);
            $conversation = $ex->getPayload();
            $responseCode = Response::HTTP_NOT_ACCEPTABLE;
        }

        $context = new Context();
        $groups = ['opportunity'];
        $context->setGroups($groups);
        $view = $this->view($conversation, $responseCode);
        $view->setContext($context);

        return $this->handleView($view);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Reply an opportunity",
     *     @SWG\Items(ref=@Model(type=Conversation::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=204,
     *     description="Request success empty content",
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=406,
     *     description="Error in form validation",
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     * ),
     * @SWG\Parameter(
     *     name="body",
     *     description="....",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="messageRoot",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="content",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="company",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyLogo",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyCity",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyCountry",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="companyWebsite",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="description",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userFirstName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userLastName",
     *             type="string",
     *         ),
     *         @SWG\Property(
     *             property="userEmail",
     *             type="string",
     *         )
     *     )
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Public")
     * @ParamConverter("conversation", converter="fos_rest.request_body")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function replyConversationAction(Request $request, Conversation $conversation, $user_id, $id)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        $parent = $this->get('api.conversation_manager')->find($id);
        if(null == $parent){
            throw new HttpException(Response::HTTP_NOT_FOUND, 'Conversation not found');

        }
        $message = new Conversation();
        try {
            $form = $this->createForm(ConversationType::class, $conversation, ['method' => $request->getMethod()]);
            $form->handleRequest($request);
            $this->get('ee.form.validator')->validate($form);
            $message->setUserId($user_id);
            $message->setMessageRoot($conversation->getMessageRoot());
            $message->setParent($parent);
            $message->setStatus("publishRequest");
            $message->setCompany($conversation->getCompany());
            $message->setCompanyName($conversation->getCompanyName());
            $message->setContent($conversation->getContent());
            $message->setCompanyCity($conversation->getCompanyCity());
            $message->setCompanyCountry($conversation->getCompanyLogo());
            $message->setCompanyCountry($conversation->getCompanyCountry());
            $message->setCompanyWebsite($conversation->getCompanyWebsite());
            $message->setDescription($conversation->getDescription());
            $message->setUserFirstName($conversation->getUserFirstName());
            $message->setUserLastName($conversation->getUserLastName());
            $message->setUserEmail($conversation->getUserEmail());
            $message->setCompanyLogo($conversation->getCompanyLogo());
            $this->get('api.conversation_manager')->save($message);

        }catch(FormBusinessException $ex) {
            foreach ($ex->getPayload() as $value){
                $logger->logInfo($value[0]->getMessage());
            }
            $logger->logError($ex->getMessage(), $ex);
            $message = $ex->getPayload();
            $responseCode = Response::HTTP_NOT_ACCEPTABLE;
        }

        $context = new Context();
        $groups = ['opportunity'];
        $context->setGroups($groups);
        $view = $this->view($message, $responseCode);
        $view->setContext($context);

        return $this->handleView($view);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Return list of conversations",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Conversation::class, groups={"opportunity"}))
     *     ),
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @Rest\QueryParam(name="limit", strict=false,  nullable=true)
     * @Rest\QueryParam(name="offset", strict=false,  nullable=true)
     * @SWG\Tag(name="Public")
     * @param ParamFetcher $paramFetcher
     * @throws \Exception
     * @return \FOS\RestBundle\View\View
     */
    public function userDiscussionAction(ParamFetcher $paramFetcher, $user_id)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        try {
            $opportunities = $this->get('api.conversation_manager')->getUserConversations($paramFetcher, $user_id);

        } catch(BusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $opportunities = $ex->getPayload();
            $responseCode = Response::HTTP_BAD_REQUEST;
        }

        $context = new Context();
        $groups = ['opportunity'];
        $context->setGroups($groups);
        $view = $this->view($opportunities, $responseCode);
        $view->setContext($context);

        return $this->handleView($view);
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Return list of conversations",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Conversation::class, groups={"opportunity"}))
     *     ),
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @Rest\QueryParam(name="limit", strict=false,  nullable=true)
     * @Rest\QueryParam(name="offset", strict=false,  nullable=true)
     * @SWG\Tag(name="Public")
     * @param ParamFetcher $paramFetcher
     * @throws \Exception
     * @return \FOS\RestBundle\View\View
     */
    public function discussionFileAction(ParamFetcher $paramFetcher, $user_id, $id)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        try {
            $opportunities = $this->get('api.conversation_manager')->getUserConversations($paramFetcher, $user_id, $id);

        } catch(BusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $opportunities = $ex->getPayload();
            $responseCode = Response::HTTP_BAD_REQUEST;
        }

        $context = new Context();
        $groups = ['opportunity'];
        $context->setGroups($groups);
        $view = $this->view($opportunities, $responseCode);
        $view->setContext($context);

        return $this->handleView($view);
    }
}