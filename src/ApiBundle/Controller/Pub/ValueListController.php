<?php

namespace ApiBundle\Controller\Pub;

use Ee\EeCommonBundle\Exception\BusinessException;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use BusinessBundle\Entity\ValueList;

class ValueListController extends FOSRestController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Return list of value-list",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=ValueList::class))
     *     ),
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     *
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *   name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @Rest\QueryParam(name="domain", strict=false,  nullable=true)
     * @Rest\QueryParam(name="limit", strict=false,  nullable=true)
     * @Rest\QueryParam(name="offset", strict=false,  nullable=true)
     * @SWG\Tag(name="Public")
     * @param ParamFetcher $paramFetcher
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function listAction(ParamFetcher $paramFetcher)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        try {
            $valuesList = $this->get('api.value_list_manager')->getValueList($paramFetcher);

        } catch(BusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $valuesList = $ex->getPayload();
            $responseCode = Response::HTTP_BAD_REQUEST;
        }

        $context = new Context();
        $groups = ['value_list'];
        $context->setGroups($groups);
        $view = $this->view($valuesList, $responseCode);
        $view->setContext($context);

        return $this->handleView($view);
    }
}