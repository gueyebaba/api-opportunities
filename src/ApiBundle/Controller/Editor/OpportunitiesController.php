<?php

namespace ApiBundle\Controller\Editor;

use ApiBundle\Form\OpportunityParametersType;
use BusinessBundle\Entity\Opportunity;
use ApiBundle\Form\OpportunityType;
use BusinessBundle\ValueObject\OpportunityParameters;
use Ee\EeCommonBundle\Exception\BusinessException;
use Ee\EeCommonBundle\Service\Validation\Form\FormBusinessException;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Ee\EeCommonBundle\Exception\DefaultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


class OpportunitiesController extends FOSRestController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="return list of opportunities",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Opportunity::class, groups={"opportunity"}))
     *     ),
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *      name="X-CUSTOMER-REF",
     *      in="header",
     *      type="string",
     *      required=true,
     * ),
     * @SWG\Parameter(
     *      name="X-SCOPE",
     *      in="header",
     *      type="string",
     *      required=true,
     * ),
     * @SWG\Parameter(
     *      name="login",
     *      in="header",
     *      type="string",
     *      required=true,
     * ),
     * @SWG\Parameter(
     *      name="password",
     *      in="header",
     *      type="string",
     *      required=true,
     * ),
     * @SWG\Parameter(
     *      name="industries",
     *      in="query",
     *      type="array",
     *      @SWG\Items(
     *          type="string"
     *      )
     * ),
     * @SWG\Parameter(
     *      name="counterparts",
     *      in="query",
     *      type="array",
     *      @SWG\Items(
     *          type="string"
     *      )
     * )
     * @Rest\QueryParam(name="userEmail", strict=false,  nullable=true)
     * @Rest\QueryParam(name="companyName", strict=false,  nullable=true)
     * @Rest\QueryParam(name="creationDate", strict=false,  nullable=true)
     * @Rest\QueryParam(name="location", strict=false,  nullable=true)
     * @Rest\QueryParam(name="limit", strict=false,  nullable=true)
     * @Rest\QueryParam(name="offset", strict=false,  nullable=true)
     * @Rest\QueryParam(name="closingDateFrom", strict=false, nullable=true)
     * @Rest\QueryParam(name="closingDateTo", strict=false, nullable=true)
     * @Rest\QueryParam(name="sortBy", allowBlank=false, default="creationDate", description="Sort field")
     * @Rest\QueryParam(name="sortDir", requirements="(asc|desc)+", allowBlank=false, default="desc", description="Sort direction")
     * @SWG\Tag(name="Editor")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function listAction(Request $request)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');

        try {
            $opportunityParameters = $this->get('api.opportunity_value_object');
            $defaultOffset = $opportunityParameters->getOffset();
            $defaultLimit = $opportunityParameters->getLimit();

            $form = $this->createForm(OpportunityParametersType::class, $opportunityParameters, ['method' => $request->getMethod()]);
            $form->handleRequest($request);
            $this->get('ee.form.validator')->validate($form);
            $filterParams = $opportunityParameters->toArray();
            $query = $this->get('api.opportunity_manager')->getOpportunities($filterParams);
            $limit = (empty($opportunityParameters->getLimit())) ? $defaultLimit : $opportunityParameters->getLimit();
            $offset = (empty($opportunityParameters->getOffset())) ? $defaultOffset : $opportunityParameters->getOffset();
            $context = new Context();
            $groups = ['opportunity', 'message'];
            $context->setGroups($groups);
            $paginator  = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query,
                (int)($offset / $limit) + 1,
                $limit

            );

            $opportunities = $pagination->getItems();

            $response =[
                "total" => $pagination->getTotalItemCount(),
                "opportunities" => $opportunities
            ];

            $view = $this->view($response, $responseCode);
            $view->setContext($context);


            return $this->handleView($view);

        } catch(FormBusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $opportunities = $ex->getPayload();
            $responseCode = Response::HTTP_NOT_ACCEPTABLE;
        }

        return $this->view($opportunities, $responseCode);
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="return return one opportunity by id",
     *     @SWG\Items(ref=@Model(type=Opportunity::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Editor")
     * @ParamConverter("opportunity", converter="doctrine.orm")
     * @Rest\View(serializerEnableMaxDepthChecks=true, serializerGroups={"opportunity"})
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function getOneAction(Opportunity $opportunity)
    {
        return $this->view($opportunity);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Update opportunity status",
     *     @SWG\Items(ref=@Model(type=Opportunity::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=406,
     *     description="Error in form validation",
     *
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *     name="body",
     *     description="....",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *         )
     *     )
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Editor")
     * @Rest\View(serializerEnableMaxDepthChecks=true, serializerGroups={"opportunity"})
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function updateAcceptedAction(Request $request, Opportunity $opportunity)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        try {
            $form = $this->createForm(OpportunityType::class, $opportunity, ['method' => $request->getMethod()]);
            $form->handleRequest($request);
            $this->get('ee.form.validator')->validate($form);
            $opportunity->setPublicationDate(\DateTime::class);
            $this->get('app_logger')->logInfo('Update Status', ['Title' => $opportunity->getTitle(), 'Status' => $opportunity->getStatus()]);
            $this->get('api.opportunity_manager')->save($opportunity);

        } catch(FormBusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $opportunity = $ex->getPayload();
            $responseCode = Response::HTTP_NOT_ACCEPTABLE;
        }

        return $this->view($opportunity, $responseCode);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Update opportunity status",
     *     @SWG\Items(ref=@Model(type=Opportunity::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=406,
     *     description="Error in form validation",
     *
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *     name="body",
     *     description="....",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *         )
     *     )
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Editor")
     * @ParamConverter("opportunity", converter="doctrine.orm")
     * @Rest\View(serializerEnableMaxDepthChecks=true, serializerGroups={"opportunity"})
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function updateRejectedAction(Request $request, Opportunity $opportunity)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        try {
            $form = $this->createForm(OpportunityType::class, $opportunity, ['method' => $request->getMethod()]);
            $form->handleRequest($request);
            $this->get('ee.form.validator')->validate($form);
            $this->get('app_logger')->logInfo('Update Status', ['Title' => $opportunity->getTitle(), 'Status' => $opportunity->getStatus()]);
            $this->get('api.opportunity_manager')->save($opportunity);

        } catch(FormBusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $opportunity = $ex->getPayload();
            $responseCode = Response::HTTP_NOT_ACCEPTABLE;
        }

        return $this->view($opportunity, $responseCode);
    }
}