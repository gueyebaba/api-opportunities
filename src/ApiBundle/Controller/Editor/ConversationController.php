<?php

namespace ApiBundle\Controller\Editor;

use ApiBundle\Form\ConversationType;
use BusinessBundle\Entity\Conversation;
use Ee\EeCommonBundle\Exception\BusinessException;
use Ee\EeCommonBundle\Service\Validation\Form\FormBusinessException;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Context\Context;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\HttpException;


class ConversationController extends FOSRestController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="Return list of conversations",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Conversation::class, groups={"opportunity"}))
     *     ),
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @Rest\QueryParam(name="limit", strict=false,  nullable=true)
     * @Rest\QueryParam(name="offset", strict=false,  nullable=true)
     * @SWG\Tag(name="Editor")
     * @param ParamFetcher $paramFetcher
     * @throws \Exception
     * @return \FOS\RestBundle\View\View
     */
    public function listAction(ParamFetcher $paramFetcher, $id)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        try {
            $conversations = $this->get('api.conversation_manager')->getConversations($paramFetcher ,$id);
        } catch(BusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $conversations = $ex->getPayload();
            $responseCode = Response::HTTP_BAD_REQUEST;
        }

        $context = new Context();
        $groups = ['opportunity'];
        $context->setGroups($groups);
        $view = $this->view($conversations, $responseCode);
        $view->setContext($context);

        return $this->handleView($view);
    }


    /**
     * @SWG\Response(
     *     response=200,
     *     description="Return conversation detail",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Conversation::class, groups={"opportunity"}))
     *     ),
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Editor")
     * @throws \Exception
     * @return \FOS\RestBundle\View\View
     */
    public function detailAction($opportunity_id, $id)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        try {
            $conversation = $this->get('api.conversation_manager')->getDetail($opportunity_id, $id);
        } catch(BusinessException $ex) {
            $logger->logError($ex->getMessage(), $ex);
            $conversation = $ex->getPayload();
            $responseCode = Response::HTTP_BAD_REQUEST;
        }

        $context = new Context();
        $groups = ['opportunity'];
        $context->setGroups($groups);
        $view = $this->view($conversation, $responseCode);
        $view->setContext($context);

        return $this->handleView($view);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Update message status if it's status is publishRequest",
     *     @SWG\Items(ref=@Model(type=Conversation::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=406,
     *     description="Error in form validation",
     *
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *     name="body",
     *     description="....",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *         )
     *     )
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Editor")
     * @ParamConverter("conversation", converter="doctrine.orm")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function updateAcceptedAction(Request $request, Conversation $conversation)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        try {
            if($conversation->getStatus() === Conversation::UPDATE_ACCEPT_STATUS) {
                $form = $this->createForm(ConversationType::class, $conversation, ['method' => $request->getMethod()]);
                $form->handleRequest($request);
                $this->get('ee.form.validator')->validate($form);
                $this->get('api.conversation_manager')->save($conversation);
            }
        } catch(FormBusinessException $ex) {
            $logger->logInfo($ex->getMessage(), $ex->getPayload());
            $logger->logError($ex->getMessage(), $ex);
            $conversation = $ex->getPayload();
            $responseCode = Response::HTTP_NOT_ACCEPTABLE;
        }

        $context = new Context();
        $groups = ['opportunity'];
        $context->setGroups($groups);
        $view = $this->view($conversation, $responseCode);
        $view->setContext($context);

        return $this->handleView($view);
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Update conversation status if it's refused",
     *     @SWG\Items(ref=@Model(type=Conversation::class, groups={"opportunity"}))
     * ),
     * @SWG\Response(
     *     response=403,
     *     description="Forbidden",
     *     examples={
     *          "invalid username/password":{
     *              "message": "Invalid credentials."
     *          },
     *          "Invalid customer ref/scope":{
     *              "message": "Access Denied"
     *          },
     *     }
     * ),
     * @SWG\Response(
     *     response=406,
     *     description="Error in form validation",
     *
     * ),
     * @SWG\Response(
     *     response=500,
     *     description="Technical error",
     *
     * ),
     * @SWG\Parameter(
     *     name="body",
     *     description="....",
     *     in="body",
     *     @SWG\Schema(
     *         @SWG\Property(
     *             property="status",
     *             type="string",
     *         )
     *     )
     * ),
     * @SWG\Parameter(
     *  name="X-CUSTOMER-REF",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="X-SCOPE",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="login",
     *  in="header",
     *  type="string",
     *  required=true,
     * ),
     * @SWG\Parameter(
     *  name="password",
     *  in="header",
     *  type="string",
     *  required=true,
     * )
     * @SWG\Tag(name="Editor")
     * @ParamConverter("conversation", converter="doctrine.orm")
     * @return \FOS\RestBundle\View\View
     * @throws \Exception
     */
    public function updateRejectedAction(Request $request, Conversation $conversation)
    {
        $responseCode = Response::HTTP_OK;
        $logger = $this->get('ee.app.logger');
        try {
            if($conversation->getStatus() === Conversation::UPDATE_REJECT_STATUS) {
                $form = $this->createForm(ConversationType::class, $conversation, ['method' => $request->getMethod()]);
                $form->handleRequest($request);
                $this->get('ee.form.validator')->validate($form);
                $this->get('api.conversation_manager')->save($conversation);
            }

        } catch(FormBusinessException $ex) {
            $logger->logInfo($ex->getMessage(), $ex->getPayload());
            $logger->logError($ex->getMessage(), $ex);
            $conversation = $ex->getPayload();
            $responseCode = Response::HTTP_NOT_ACCEPTABLE;
        }

        $context = new Context();
        $groups = ['opportunity'];
        $context->setGroups($groups);
        $view = $this->view($conversation, $responseCode);
        $view->setContext($context);

        return $this->handleView($view);
    }
}