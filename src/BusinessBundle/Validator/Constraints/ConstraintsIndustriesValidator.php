<?php

namespace BusinessBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;


class ConstraintsIndustriesValidator extends ConstraintValidator
{
    protected $industriesPath;
    protected $entityManager;

    /**
     * IndustriesCounterPartValidator constructor.
     * @param EntityManagerInterface $entityManager
     * @param string $industriesPath
     */
    public function __construct(EntityManagerInterface $entityManager, string $industriesPath)
    {
        $this->industriesPath = $industriesPath;
        $this->entityManager = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {

        return;
        $notValid = false;
        $contentValues = [];
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }
        try {
            $statement = $this->getConnection()->prepare("SELECT id FROM value_list");
            $statement->execute();
            $values = $statement->fetchAll();
        } catch (\Exception $exception) {
            printf('An error is occured', $exception->getMessage());
        }
        foreach ($values as $key => $valueContent){
            $contentValues[$key] = $valueContent['id'];
        }
        $values = explode(",", $value);

        foreach ($values as $key => $value) {
            if (!in_array($values[$key], $contentValues)) {
                $notValid = true;
            }
        }
        if ($notValid){
            $this->context->addViolation($constraint->message);
        }
    }


    /**
     * @return \Doctrine\DBAL\Connection
     */
    public function getConnection()
    {
        return $this->entityManager->getConnection();
    }
}