<?php

namespace BusinessBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


class ConstraintsIndustries  extends Constraint
{
    public $message = 'Industries not valid, please request GET /api/pub/value-lists for correct value';

    public function validatedBy()
    {
        return 'industries_not_valid';
    }
}