<?php

namespace BusinessBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


class ConstraintsOpportunityStatus  extends Constraint
{

    public $message = 'Status not valid: draft, published, publishRequest, refused, archived are permitted';

    public function validatedBy()
    {
        return 'status_opportunity_not_valid';
    }
}