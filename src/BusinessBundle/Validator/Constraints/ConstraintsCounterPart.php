<?php

namespace BusinessBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


class ConstraintsCounterPart  extends Constraint
{
    public $message = 'Counter part value not valid, please request GET /api/pub/value-lists for correct value';

    public function validatedBy()
    {
        return 'counter_part_not_valid';
    }
}