<?php

namespace BusinessBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


class ConstraintsConversationStatus  extends Constraint
{
    public $message = 'Status not valid: publishRequest, published, refused are permitted';

    public function validatedBy()
    {
        return 'status_conversation_not_valid';
    }
}