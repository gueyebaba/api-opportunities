<?php

namespace BusinessBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Doctrine\ORM\EntityManagerInterface;


class ConstraintsCounterPartValidator extends ConstraintValidator
{
    protected $counterPartPath;
    protected $entityManager;

    /**
     * ConstraintsCounterPartValidator constructor.
     * @param EntityManagerInterface $entityManager
     * @param string $counterPartPath
     */
    public function __construct(EntityManagerInterface $entityManager, string $counterPartPath)
    {
        $this->counterPartPath = $counterPartPath;
        $this->entityManager = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        return;
        $notValid = false;
        $contentValues = [];
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedTypeException($value, 'string');
        }
        try {
            $statement = $this->getConnection()->prepare("SELECT id FROM value_list");
            $statement->execute();
            $values = $statement->fetchAll();
        } catch (\Exception $exception) {
            printf('An error is occured', $exception->getMessage());
        }
        foreach ($values as $key => $valueContent){
            $contentValues[$key] = $valueContent['id'];
        }
        $values = explode(",", $value);

        foreach ($values as $key => $value) {
            if (!in_array($values[$key], $contentValues)) {
                $notValid = true;
            }
        }
        if ($notValid){
            $this->context->addViolation($constraint->message);
        }
    }

    /**
     * @return \Doctrine\DBAL\Connection
     */
    public function getConnection()
    {
        return $this->entityManager->getConnection();
    }
}