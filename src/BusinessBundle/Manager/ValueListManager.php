<?php

namespace BusinessBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use BusinessBundle\Manager\BaseManager as BaseManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ValueListManager
 */
class ValueListManager extends BaseManager
{
    protected $defaultLimit;
    protected $defaultOffset;

    /**
     * ValueListManager constructor.
     * @param EntityManagerInterface $em
     * @param string $className
     * @param int $defaultLimit
     * @param int $defaultOffset
     */
    public function __construct(EntityManagerInterface $em, string $className, int $defaultLimit, int $defaultOffset)
    {
        parent::__construct($em, $className);
        $this->defaultLimit = $defaultLimit;
        $this->defaultOffset = $defaultOffset;
    }

    /**
     * @param $paramFetcher
     * @return mixed
     */
    public function getValueList($paramFetcher)
    {
        $listValues =  $this->repository->getValueList([
            'offset'=>$paramFetcher->get('offset'),
            'limit'=>$paramFetcher->get('limit'),
            'domain'=>$paramFetcher->get('domain'),
            'defaultOffset'=>$this->defaultOffset,
            'defaultLimit'=>$this->defaultLimit
        ]);

        return $listValues;
    }
}
