<?php

namespace BusinessBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use BusinessBundle\Manager\BaseManager as BaseManager;

/**
 * Class UserRefManager
 */
class UserPrefManager extends BaseManager
{
    protected $defaultLimit;
    protected $defaultOffset;

    /**
     * UserRefManager constructor.
     * @param EntityManagerInterface $em
     * @param string $className
     * @param int $defaultLimit
     * @param int $defaultOffset
     */
    public function __construct(EntityManagerInterface $em, string $className, int $defaultLimit, int $defaultOffset)
    {
        parent::__construct($em, $className);
        $this->defaultLimit = $defaultLimit;
        $this->defaultOffset = $defaultOffset;
    }

    /**
     * @param $paramFetcher
     * @return mixed
     */
    public function getFavoriteOpportunities($paramFetcher)
    {
        $usersRef =  $this->repository->getFavoriteOpportunities([
            'offset'=>$paramFetcher->get('offset'),
            'limit'=>$paramFetcher->get('limit'),
            'defaultOffset'=>$this->defaultOffset,
            'defaultLimit'=>$this->defaultLimit
        ]);

        return $usersRef;
    }

    /**
     * @param $paramFetcher
     * @param $id
     * @return mixed
     */
    public function getUserBookMarks($paramFetcher, $user_id)
    {
        $bookMarks =  $this->repository->getUserBookMarks([
            'offset'=>$paramFetcher->get('offset'),
            'limit'=>$paramFetcher->get('limit'),
            'defaultOffset'=>$this->defaultOffset,
            'defaultLimit'=>$this->defaultLimit
        ], $user_id);

        return $bookMarks;
    }
}
