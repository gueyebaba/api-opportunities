<?php

namespace BusinessBundle\Manager;

use BusinessBundle\Entity\conversation;
use Doctrine\ORM\EntityManagerInterface;
use BusinessBundle\Manager\BaseManager as BaseManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;


/**
 * Class ValueListManager
 */
class ConversationManager extends BaseManager
{
    protected $defaultLimit;
    protected $defaultOffset;

    /**
     * OpportunityManager constructor.
     * @param EntityManagerInterface $em
     * @param string $className
     * @param int $defaultLimit
     * @param int $defaultOffset
     */
    public function __construct(EntityManagerInterface $em,  string $className, int $defaultLimit, int $defaultOffset)
    {
        parent::__construct($em, $className);
        $this->defaultLimit = $defaultLimit;
        $this->defaultOffset = $defaultOffset;
    }

    /**
     * @param $paramFetcher
     * @param $id
     * @return mixed
     */
    public function getConversations($paramFetcher, $id)
    {
        $conversations =  $this->repository->getConversations([
            'offset'=>$paramFetcher->get('offset'),
            'limit'=>$paramFetcher->get('limit'),
            'defaultOffset'=>$this->defaultOffset,
            'defaultLimit'=>$this->defaultLimit
        ], $id);

        return $conversations;
    }

    /**
     * @param $opportunity_id
     * @param $id
     * @return mixed
     */
    public function getDetail($opportunity_id, $id)
    {
        $conversations =  $this->repository->getDetail($opportunity_id, $id);

        return $conversations;
    }

    /**
     * @param $paramFetcher
     * @param $id
     * @return mixed
     */
    public function getUserConversations($paramFetcher, $user_id)
    {
        $conversations =  $this->repository->getUserConversations([
            'offset'=>$paramFetcher->get('offset'),
            'limit'=>$paramFetcher->get('limit'),
            'defaultOffset'=>$this->defaultOffset,
            'defaultLimit'=>$this->defaultLimit
        ], $user_id);

        return $conversations;
    }


    /**
     * @param $paramFetcher
     * @param $user_id
     * @param $id
     * @return mixed
     */
    public function getdiscussionFile($paramFetcher, $user_id, $id)
    {
        $conversations =  $this->repository->getdiscussionFile([
            'offset'=>$paramFetcher->get('offset'),
            'limit'=>$paramFetcher->get('limit'),
            'defaultOffset'=>$this->defaultOffset,
            'defaultLimit'=>$this->defaultLimit
        ], $user_id, $id);

        return $conversations;
    }
}
