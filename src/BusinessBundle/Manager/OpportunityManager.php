<?php

namespace BusinessBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use BusinessBundle\Manager\BaseManager as BaseManager;


/**
 * Class OpportunityManager
 */
class OpportunityManager extends BaseManager
{
    protected $defaultLimit;
    protected $defaultOffset;

    /**
     * OpportunityManager constructor.
     * @param EntityManagerInterface $em
     * @param string $className
     * @param int $defaultLimit
     * @param int $defaultOffset
     */
    public function __construct(EntityManagerInterface $em, string $className, int $defaultLimit, int $defaultOffset)
    {
        parent::__construct($em, $className);
        $this->defaultLimit = $defaultLimit;
        $this->defaultOffset = $defaultOffset;
    }

    /**
     * @param $filterParams
     * @return mixed
     */
    public function getOpportunities($filterParams)
    {
        $opportunities =  $this->repository->getOpportunities($filterParams);

        return $opportunities;
    }
}
