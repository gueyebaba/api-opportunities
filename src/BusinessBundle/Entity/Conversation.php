<?php

namespace BusinessBundle\Entity;
use JMS\Serializer\Annotation as JMS;
use Swagger\Annotations as SWG;

/**
 * Conversation
 */
class Conversation
{
    const UPDATE_REJECT_STATUS = "refused";
    const UPDATE_ACCEPT_STATUS = "publishRequest";
    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var int
     */
    private $id;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $messageRoot;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $content;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var \DateTime
     */
    private $publicationDate;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $status;

    /**
     * @var $opportunity
     */
    private $opportunity;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $image;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $company;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $companyLogo;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $companyName;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $companyCity;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $companyCountry;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $companyWebsite;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $description;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $userFirstName;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $userLastName;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $userEmail;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @SWG\Property(description="The unique identifier of the user who create the conversation.", example="1")
     * @var string
     */
    private $userId;

    private $lft;

    private $lvl;

    private $rgt;

    private $root;

    private $parent;


    /**
     * @JMS\Groups(groups={"opportunity"})
     */
    private $children;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set messageRoot
     *
     * @param string $messageRoot
     *
     * @return $this
     */
    public function setMessageRoot($messageRoot)
    {
        $this->messageRoot = $messageRoot;

        return $this;
    }

    /**
     * Get messageRoot
     *
     * @return string
     */
    public function getMessageRoot()
    {
        return $this->messageRoot;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     *
     * @return $this
     */
    public function setCreationDate()
    {
        $this->creationDate = new \DateTime();

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @return string
     */
    public function getOpportunity()
    {
        return $this->opportunity;
    }

    /**
     * @param string $opportunity
     * @return $this
     */
    public function setOpportunity($opportunity)
    {
        $this->opportunity = $opportunity;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    /**
     * @param \DateTime $publicationDate
     * @return $this
     */
    public function setPublicationDate($publicationDate)
    {
        $this->publicationDate = $publicationDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     * @return $this
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyLogo()
    {
        return $this->companyLogo;
    }

    /**
     * @param mixed $companyLogo
     * @return $this
     */
    public function setCompanyLogo($companyLogo)
    {
        $this->companyLogo = $companyLogo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     * @return $this
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyCity()
    {
        return $this->companyCity;
    }

    /**
     * @param mixed $companyCity
     * @return $this
     */
    public function setCompanyCity($companyCity)
    {
        $this->companyCity = $companyCity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyCountry()
    {
        return $this->companyCountry;
    }

    /**
     * @param mixed $companyCountry
     * @return $this
     */
    public function setCompanyCountry($companyCountry)
    {
        $this->companyCountry = $companyCountry;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyWebsite()
    {
        return $this->companyWebsite;
    }

    /**
     * @param mixed $companyWebsite
     * @return $this
     */
    public function setCompanyWebsite($companyWebsite)
    {
        $this->companyWebsite = $companyWebsite;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserFirstName()
    {
        return $this->userFirstName;
    }

    /**
     * @param mixed $userFirstName
     * @return $this
     */
    public function setUserFirstName($userFirstName)
    {
        $this->userFirstName = $userFirstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserLastName()
    {
        return $this->userLastName;
    }

    /**
     * @param mixed $userLastName
     * @return $this
     */
    public function setUserLastName($userLastName)
    {
        $this->userLastName = $userLastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * @param mixed $userEmail
     * @return $this
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * @param mixed $lft
     */
    public function setLft($lft)
    {
        $this->lft = $lft;
    }

    /**
     * @return mixed
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * @param mixed $lvl
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;
    }

    /**
     * @return mixed
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * @param mixed $rgt
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;
    }

    /**
     * @return mixed
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @param mixed $root
     */
    public function setRoot($root)
    {
        $this->root = $root;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }
}
