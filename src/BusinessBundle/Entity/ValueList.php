<?php

namespace BusinessBundle\Entity;
use JMS\Serializer\Annotation as JMS;

/**
 * ValueList
 */
class ValueList
{
    /**
     * @JMS\Groups(groups={"value_list","opportunity"})
     * @var string
     */
    private $id;

    /**
     * @JMS\Groups(groups={"value_list","opportunity"})
     * @var string
     */
    private $domain;

    /**
     * @JMS\Groups(groups={"value_list","opportunity"})
     * @var string
     */
    private $key;

    /**
     * @JMS\Groups(groups={"value_list","opportunity"})
     * @var string
     */
    private $value;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param string $domain
     * @return ValueList
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     * @return ValueList
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return ValueList
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}
