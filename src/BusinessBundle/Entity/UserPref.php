<?php

namespace BusinessBundle\Entity;

use JMS\Serializer\Annotation as JMS;
use Swagger\Annotations as SWG;

/**
 * UserPref
 */
class UserPref
{
    const TYPE_BOOKMARK = "bookmark";
    /**
     * @var int
     * @JMS\Groups(groups={"bookmark"})
     */
    private $id;

    /**
     * @var string
     * @JMS\Groups(groups={"bookmark"})
     */
    private $userRef;

    /**
     * @var string
     * @JMS\Groups(groups={"bookmark"})
     */
    private $type;

    private $deletedAt;

    /**
     * @JMS\Groups(groups={"bookmark"})
     * @JMS\Type("array<integer>")
     * @var array
     */
    private $data;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userRef
     *
     * @param string $userRef
     *
     * @return UserPref
     */
    public function setUserRef($userRef)
    {
        $this->userRef = $userRef;

        return $this;
    }

    /**
     * Get userRef
     *
     * @return string
     */
    public function getUserRef()
    {
        return $this->userRef;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return UserPref
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param $deletedAt
     * @return $this
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }
}

