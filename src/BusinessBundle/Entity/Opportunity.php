<?php

namespace BusinessBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;
use Swagger\Annotations as SWG;


/**
 * Opportunity
 */
class Opportunity
{
    const UPDATE_STATUS = "publishRequest";
    const EXPIRATION_DATE_STATUS_PUBLISH = "published";
    const EXPIRATION_DATE_STATUS_EXPIRED = "expired";
    const DELETE_STATUS = "draft";

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @SWG\Property(description="The unique identifier of the opportunity.", example="1")
     * @var int
     */
    private $id;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @SWG\Property(description="The unique identifier of the user who create the opportunity.", example="1")
     * @var string
     */
    private $userId;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @SWG\Property(description="Title the opportunity.", example="opp title")
     * @var string
     */
    private $title;


    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $fullDescription;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @JMS\Type("array<string>")
     */
    private $tags;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @JMS\Type("array<BusinessBundle\Entity\ValueList>")
     */
    private $industries;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $typeOfDesired;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @JMS\Type("array<BusinessBundle\Entity\ValueList>")
     */
    private $counterparts;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $media;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $website;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $linkToSocialMedia;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $location;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var string
     */
    private $country;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var \DateTime
     */
    private $publicationDate;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var \DateTime
     */
    private $closedAt;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var \DateTime
     */
    private $publishRequestAt;

    /**
     * @JMS\Groups(groups={"opportunity"})
     */
    private $status;

    private $deletedAt;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * @var ArrayCollection
     */
    private $conversations;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * var string
     */
    private $companyLogo;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * var string
     */
    private $companyName;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * var string
     */
    private $companyWebsite;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * var string
     */
    private $companyCity;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * var string
     */
    private $companyCountry;

    /**
     * var string
     * @JMS\Groups(groups={"opportunity"})
     */
    private $companyDescription;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * var string
     */
    private $userFirstName;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * var string
     */
    private $userLastName;

    /**
     * @JMS\Groups(groups={"opportunity"})
     * var string
     */
    private $userEmail;

    private $valueLists;

    public function __construct() {
        $this->conversations = new ArrayCollection();

    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set fullDescription
     *
     * @param string $fullDescription
     *
     * @return $this
     */
    public function setFullDescription($fullDescription)
    {
        $this->fullDescription = $fullDescription;

        return $this;
    }

    /**
     * Get fullDescription
     *
     * @return string
     */
    public function getFullDescription()
    {
        return $this->fullDescription;
    }

    /**
     * Set tags
     *
     * @param array $tags
     *
     * @return $this
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set typeOfDesired
     *
     * @param array $typeOfDesired
     *
     * @return $this
     */
    public function setTypeOfDesired($typeOfDesired)
    {
        $this->typeOfDesired = $typeOfDesired;

        return $this;
    }

    /**
     * Get typeOfDesired
     *
     * @return array
     */
    public function getTypeOfDesired()
    {
        return $this->typeOfDesired;
    }

    /**
     * Set counterparts
     * @param ArrayCollection $valueLists
     * @return Opportunity
     */
    public function setCounterparts($counterparts)
    {
        $this->counterparts = $counterparts;

        return $this;
    }

    /**
     * Get counterparts
     *
     * @return int
     */
    public function getCounterparts()
    {
        return $this->counterparts;
    }

    /**
     * Set media
     *
     * @param string $media
     *
     * @return $this
     */
    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return string
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return $this
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set linkToSocialMedia
     *
     * @param string $linkToSocialMedia
     *
     * @return $this
     */
    public function setLinkToSocialMedia($linkToSocialMedia)
    {
        $this->linkToSocialMedia = $linkToSocialMedia;

        return $this;
    }

    /**
     * Get linkToSocialMedia
     *
     * @return string
     */
    public function getLinkToSocialMedia()
    {
        return $this->linkToSocialMedia;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return $this
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return $this
     */
    public function setCreationDate()
    {
        $this->creationDate = new \DateTime();

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @return $this
     */
    public function setModificationDate()
    {
        $this->modificationDate = new \DateTime();

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * @return $this
     */
    public function setPublicationDate()
    {
        $this->publicationDate = new \DateTime();

        return $this;
    }

    /**
     * Get publicationDate
     *
     * @return \DateTime
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    /**
     * Set status
     *
     * @param array $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return array
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getConversations()
    {
        return $this->conversations;
    }

    /**
     * Add conversation
     *
     * @param \BusinessBundle\Entity\Conversation $conversation
     *
     * @return $this
     */
    public function addConversation(\BusinessBundle\Entity\Conversation $conversation)
    {
        $this->conversations[] = $conversation;

        return $this;
    }

    /**
     * Remove conversation
     *
     * @param \BusinessBundle\Entity\Conversation $conversation
     */
    public function removeConversation(\BusinessBundle\Entity\Conversation $conversation)
    {
        $this->conversations->removeElement($conversation);
    }

    /**
     * @return mixed
     */
    public function getCompanyLogo()
    {
        return $this->companyLogo;
    }

    /**
     * @param mixed $companyLogo
     * @return $this
     */
    public function setCompanyLogo($companyLogo)
    {
        $this->companyLogo = $companyLogo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     * @return $this
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyWebsite()
    {
        return $this->companyWebsite;
    }

    /**
     * @param mixed $companyWebsite
     * @return $this
     */
    public function setCompanyWebsite($companyWebsite)
    {
        $this->companyWebsite = $companyWebsite;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyCity()
    {
        return $this->companyCity;
    }

    /**
     * @param mixed $companyCity
     * @return $this
     */
    public function setCompanyCity($companyCity)
    {
        $this->companyCity = $companyCity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyCountry()
    {
        return $this->companyCountry;
    }

    /**
     * @param mixed $companyCountry
     * @return $this
     */
    public function setCompanyCountry($companyCountry)
    {
        $this->companyCountry = $companyCountry;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyDescription()
    {
        return $this->companyDescription;
    }

    /**
     * @param mixed $companyDescription
     * @return $this
     */
    public function setCompanyDescription($companyDescription)
    {
        $this->companyDescription = $companyDescription;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserFirstName()
    {
        return $this->userFirstName;
    }

    /**
     * @param mixed $userFirstName
     * @return $this
     */
    public function setUserFirstName($userFirstName)
    {
        $this->userFirstName = $userFirstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserLastName()
    {
        return $this->userLastName;
    }

    /**
     * @param mixed $userLastName
     * @return $this
     */
    public function setUserLastName($userLastName)
    {
        $this->userLastName = $userLastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * @param mixed $userEmail
     * @return $this
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIndustries()
    {
        return $this->industries;
    }

    /**
     * @param ArrayCollection $valueLists
     * @return $this
     */
    public function setIndustries($industries)
    {
        $this->industries = $industries;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * @param mixed $deletedAt
     * @return $this
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getValueLists()
    {
        return $this->valueLists;
    }

    /**
     * @param ArrayCollection $valueLists
     * @return $this
     */
    public function setValueLists($valueLists)
    {
        $this->valueLists = $valueLists;
        return $this;
    }

    /**
     * @param ValueList $valueList
     * @return $this
     */
    public function addValueList(ValueList $valueList)
    {
        $this->valueLists[] = $valueList;

        return $this;
    }

    /**
     * @JMS\Type("array")
     * @JMS\VirtualProperty
     * @JMS\SerializedName("industries2")
     */
    public function getIndustriesAsArray()
    {
       $industries = [];
       foreach($this->industries as $industry){
           $industries[] = $industry->getkey();
       }

       return $industries;
    }

    /**
     * @JMS\Type("array")
     * @JMS\VirtualProperty
     * @JMS\SerializedName("counterparts2")
     */
    public function getCounterpartsAsArray()
    {
        $counterparts = [];
        foreach($this->counterparts as $counterpart){
            $counterparts[] = $counterpart->getkey();
        }

        return $counterparts;
    }

    /**
     * @return \DateTime
     */
    public function getClosedAt()
    {
        return $this->closedAt;
    }

    /**
     * @param \DateTime $closedAt
     * @return $this
     */
    public function setClosedAt($closedAt)
    {
        $this->closedAt = $closedAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublishRequestAt()
    {
        return $this->publishRequestAt;
    }

    /**
     * @param \DateTime $publishRequestAt
     * @return $this
     */
    public function setPublishRequestAt($publishRequestAt)
    {
        $this->publishRequestAt = $publishRequestAt;
        return $this;
    }

}
