<?php

namespace BusinessBundle\ValueObject;

use JMS\Serializer\Annotation as JMS;


class OpportunityParameters
{
    /** @var array */
    protected $industries;

    /** @var array */
    protected $counterparts;
    protected $userEmail;
    protected $companyName;
    protected $creationDate;
    protected $location;
    protected $offset;
    protected $limit;
    protected $sortBy;
    protected $sortDir;
    protected $closingDateFrom;
    protected $closingDateTo;

    /**
     * @return array
     */
    public function getIndustries()
    {
        return $this->industries;
    }

    /**
     * @param array $industries
     * @return OpportunityParameters
     */
    public function setIndustries($industries)
    {
        $this->industries = $industries;
        return $this;
    }

    /**
     * @return array
     */
    public function getCounterparts()
    {
        return $this->counterparts;
    }

    /**
     * @param array $counterparts
     * @return OpportunityParameters
     */
    public function setCounterparts($counterparts)
    {
        $this->counterparts = $counterparts;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * @param mixed $userEmail
     * @return $this
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     * @return $this
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param mixed $offset
     * @return $this
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param mixed $limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSortBy()
    {
        return $this->sortBy;
    }

    /**
     * @param mixed $sortBy
     */
    public function setSortBy($sortBy)
    {
        $this->sortBy = $sortBy;
    }

    /**
     * @return mixed
     */
    public function getSortDir()
    {
        return $this->sortDir;
    }

    /**
     * @param mixed $sortDir
     */
    public function setSortDir($sortDir)
    {
        $this->sortDir = $sortDir;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param mixed $creationDate
     * @return $this
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     * @return OpportunityParameters
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray() {
        $data = [];

        $class_vars = get_object_vars($this);

        foreach ($class_vars as $name => $value) {
            if(!empty($value))
            {
                $data[$name] = $value;
            }
        }

        return $data;
    }

    /**
     * @return mixed
     */
    public function getClosingDateFrom()
    {
        return $this->closingDateFrom;
    }

    /**
     * @param mixed $closingDateFrom
     * @return OpportunityParameters
     */
    public function setClosingDateFrom($closingDateFrom)
    {
        $this->closingDateFrom = $closingDateFrom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClosingDateTo()
    {
        return $this->closingDateTo;
    }

    /**
     * @param mixed $closingDateTo
     * @return OpportunityParameters
     */
    public function setClosingDateTo($closingDateTo)
    {
        $this->closingDateTo = $closingDateTo;
        return $this;
    }
}